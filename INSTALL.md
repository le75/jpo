# JPO

A static page generator for Le75 open doors day.

## Dependencies

- make
- jq
- libvips
- exiftool
- python-jinja2

Installing those dependencies depend on your operating system. Please refer to
its documentation.

## Usage

- create a symlink named `data` that points to your image directory
- type `make`; your website will be generated in the `site` directory
