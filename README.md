

# Gallerie Journée Portes ouvertes

- Partie générative (2j)

  - Générer des images dans différents formats (grande, vignettes)
  - Générer un template HTML de base
  - Manuel: convention de nommage, classement…
  - OPTION: permettre des vignettes sélectionnées à la main
  - OPTION: prise en compte des vidéos

- Partie navigateur (1j)

  - design générique (structure et classes CSS)
  - Filtre par étiquette
  - galerie d'images (slideshow)
  - optimisation de chargement des images

- Non pris en charge:

  - Edition des images (étiquettes, commentaires, auteurs)

Logiciels envisagés:
    - makefile (bash) ou python
    - imagemagick ou PIL (python)
    - Bash ou Jinja2 (python)
    - Javascript (Isotope, Lazyload)


# Générateur de page pour les portes ouvertes du 75

## Requirements

community/libvips

## Organisation du contenu

Un dossier data.exemple est disponible en exemple. Le générateur demande un
dossier d'images nommé `data` à la racine du projet et structuré de la façon suivante.

1. un dossier par projet: ex. `gr_affiche-citoyenne` (tout en minuscules, sans espaces ni caractère spécial)

2. dans ce dossier, une liste d'images en jpg suffixées avec un tiret-bas et un nombre à deux chiffres, ex. `affiche-citoyenne_gr2_2021_00.jpg` ou `affiche-citoyenne_gr2_2021_01.jpg`, etc. La première image doit etre nommées `projet_00.jpg` et servira de vignette dans la liste présentée sur la page.

    data
    ├── foo
    │   ├── 2_passes_3_couleurs_ip2_2020_00.jpg
    │   ├── …
    │   └── 2_passes_3_couleurs_ip2_2020_07.jpg
    └── bar

        ├── anissa_cherif_ph3_00.jpg
        ├── …
        └── anissa_cherif_ph3_05.jpg

Le plus simple est peut-être de faire un lien symbolique (alias sur mac) depuis le dossier synchronisé Nextcloud vers le projet:

    ln -s ~/Nextcloud/OSP/alex/le75/PO2023/contenus/originaux/selection-finale data

## Préparer les images

Ce qui est sûr, c'est qu'on rentrera les données directement «dans» les images, à l'aide d'EXIF. Je pense qu'on peut le faire tout simplement avec Aperçu sur OSX:

https://www.macobserver.com/tips/how-to/macos-use-preview-inspector-look-photo-metadata/

Je ne sais pas en revanche si on peut éditer plusieurs photos à la fois, mais si ce n'est pas possible il existe probablement une myriade d'outils.

Je propose donc de déjà reverser sur le nextcloud les images de l'année passée. Si tu installe le connecteur Nextcloud, tu pourras synchroniser ce dossier avec ton ordi et éditer toutes les metadonnées depuis le logiciel de ton choix. Et moi ou Romain pourrons y avoir accès également.

Qu'en dis-tu? Y a t'il un endroit privilégié pour mettre ces images sur le nuage?

Merci!

Alex

Je pense qu'il serait souhaitable de légender les images (en plus des mots-clés). Puis-je vous demander de me remettre des légende minimales pour vos images avec les infos suivantes?

Entre accolades, les infos obligatoires; entre crochet, les infos optionnelles

[Prénom Nom], {Titre du projet}, [Techniques], {Bachelier}, {Option}, {Année}

Ex:

Albertine Dupont, Un super titre, sérigraphie sur soie, B3, Peinture, 2021

> Si vous avez des légendes particulières à ajouter, veuillez me les transmettre le plus clairement possible dans votre mail de livraison.
>
>     *Format type d'un nom de fichier :*
>     /nom-du-projet_promo_annee_numero.ext/
>
>     nom-du-projet: court identifiant; minuscules, pas
>     d'accents/caractères spéciaux, espaces remplacées par des traits
>     d'union "-"
>     promo: ip/ph/pe/gr + bac; ex: "gr3"
>     annee: 4 chiffres; ex. 2021
>     numero: 2 chiffres. Commencer par "00"
>     ext: jpg/png/gif (pas de tiff, et pas besoin de très haute résolution)
>
>     Un exemple pour une image seule:
>     /nom-du-projet_ip1_2020_00.jpg/
>
>     Un exemple pour une série d'image:
>     /nom-du-projet_gr3_2022_00.jpg/
>     /nom-du-projet_gr3_2022_01.jpg/
>     /nom-du-projet_gr3_2022_02.jpg/
>     …
>
> Pour la première image d'un série, veuillez en choisir une qui soit la plus attrayante possible ou au moins lisible en petite taille (la page d'accueil du site présentera une mosaïque de miniatures de la première image de chaque série, idem pour les images seules.)
>
> Choisissez également dans la liste ci-dessous 10 mots-clés qui pourraient, de près ou de loin, résumer les spécificités des images que vous me livrerez :
>
>     Accumuler
>     Afficher
>     Assembler
>     Capturer
>     Cartographier
>     Coder
>     Combiner
>     Composer
>     Dessiner
>     Détourner
>     Documenter
>     Éditer
>     Épuiser
>     Explorer
>     Illustrer
>     Imprimer
>     Improviser
>     Multiplier
>     Plier
>     Projeter
>     Protester
>     Récupérer
>     Relier
>     Spéculer
>     Tracer
>
