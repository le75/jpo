/*
Prints the dominant color of the given image
creates a 1x1 thumbnail and read the pixel value
*/

#include <stdio.h>
#include <vips/vips.h>


int main (int argc, char **argv)
{
    if (VIPS_INIT (argv[0]))
        vips_error_exit ("unable to start VIPS");

    if( argc != 2 )
        vips_error_exit("usage: %s infile", argv[0]); 
  
    VipsImage *out = NULL;
    double *rgb_values;
    int n;

    if (vips_thumbnail (argv[1], &out, 1, NULL) 
            || vips_getpoint(out, &rgb_values, &n, 0, 0, NULL))
        vips_error_exit( NULL );

    printf("#");
    for (int i=0; i<n; i++) {
        printf("%X", (int)rgb_values[i]);
    }
    printf("\n");

    g_object_unref( out ); 

    vips_shutdown ();

    return 0;
}
