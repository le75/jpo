Compile by typing the following

```bash
LANG=C gcc -o ../bin/dominant_color -g -Wall dominant_color.c `pkg-config vips --cflags --libs`
```
