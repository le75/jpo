def fname($extra):
  (.SourceFile | sub("^data"; "img") | sub(".jpg$$"; "." + $extra + ".jpg"));

.[0] | {
    "description": .Description, 
    "versions": {
        "large": fname("1600x1600"),
        "medium": fname("0900x0900"),
        "small": fname("0600x0600"),
        }
    }
