#! /usr/bin/env python3


# Copyright (C) 2021-2022 Alexandre Leray

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# usage: generate.py [-h] [-t [TEMPLATES_DIR]] [-o [OUTFILE]] template [json]
#
# positional arguments:
#   template              the name of your template
#   json                  the json file containing the template context (read
#                         json from <stdin> if omitted)
#
# options:
#   -h, --help            show this help message and exit
#   -t [TEMPLATES_DIR], --templates-dir [TEMPLATES_DIR]
#                         the root templates dir (default: 'templates')
#   -o [OUTFILE], --outfile [OUTFILE]
#                         the file where to save the result (write html to
#                         <stdout> if omitted)


import os
import unicodedata

from jinja2 import Environment, FileSystemLoader
from itertools import groupby


def slugify(text):
    return text


def strip_accents(s):
    return "".join(
        c for c in unicodedata.normalize("NFD", s) if unicodedata.category(c) != "Mn"
    )


def groupbyletter(data):
    return groupby(data, lambda x: strip_accents(x["slug"][0]))


def render_to_string(templates_dir, template, json_ctx):
    env = Environment(loader=FileSystemLoader(templates_dir))
    env.filters["slugify"] = slugify
    env.filters["groupbyletter"] = groupbyletter
    template = env.get_template(template)
    return template.render(ctx=json_ctx)


if __name__ == "__main__":
    import argparse
    import sys
    import json

    parser = argparse.ArgumentParser()
    parser.add_argument("template", type=str, help="the name of your template")
    parser.add_argument(
        "json",
        nargs="?",
        type=argparse.FileType("r"),
        default=sys.stdin,
        help="the json file containing the template context (read json from <stdin> if omitted)",
    )
    parser.add_argument(
        "-t",
        "--templates-dir",
        nargs="?",
        type=str,
        default=os.path.join(os.getcwd(), "templates"),
        help="the root templates dir (default: 'templates')",
    )
    parser.add_argument(
        "-o",
        "--outfile",
        nargs="?",
        type=argparse.FileType("w"),
        default=sys.stdout,
        help="the file where to save the result (write html to <stdout> if omitted)",
    )
    args = parser.parse_args()
    html = render_to_string(args.templates_dir, args.template, json.load(args.json))
    args.outfile.write(html)
