def slug: ascii_downcase;

def fname($extra):
  (.SourceFile | sub("^data"; "img") | sub(".jpg$$"; $extra + ".jpg"));

{
  images: map(
    . + {
      slugs: (.Keywords // [] | map(slug)),
      thumb: fname(".thumb"),
      large: fname(".large"),
    }
  )
}
