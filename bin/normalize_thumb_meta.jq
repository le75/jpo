def fname($extra):
  (.SourceFile | sub("^site"; ""));

.[0] | { 
    "src": fname("bla"),  
    "width": .ImageWidth, 
    "height": .ImageHeight,
	"dominant": $dominant
}
