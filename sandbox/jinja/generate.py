from jinja2 import Environment, FileSystemLoader
from pathlib import Path
import os


root = os.path.dirname(os.path.abspath(__file__))
templates_dir = os.path.join(root, "templates")
env = Environment(loader=FileSystemLoader(templates_dir))
template = env.get_template("index.html")


class Image(object):

    """Docstring for MyClass. """

    def __init__(self, path):
        """TODO: to be defined.

        :path: TODO

        """
        self._path = path

    def src(self):
        return self._path

    def width(self):
        return self.width


images = [Image(path) for path in sorted(Path("data").rglob("*.jpg"))]


filename = os.path.join(root, "html", "index.html")
with open(filename, "w") as fh:
    fh.write(
        template.render(
            images=images
        )
    )
