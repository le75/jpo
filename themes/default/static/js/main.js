;(function() {
  // init thumb wall
  var elem = document.querySelector('.grid');
  var iso = new Isotope(elem, {
    itemSelector: '.grid-item',
  });

  // filters
  document.querySelector('.filters-wrapper').addEventListener("click", function(e) {
    for (var target = e.target; target && target != this; target = target.parentNode) {
      if (target.matches('.js-filter')) {
        let oldActive = this.querySelector('.is-active');

        if (oldActive) {
          oldActive.classList.remove('is-active');
          if (oldActive == target) {
            iso.arrange({ filter: '*' });
          } else {
            iso.arrange({ filter: target.dataset.filter });
            target.classList.add("is-active");
          }
        } else {
          iso.arrange({ filter: target.dataset.filter });
          target.classList.add("is-active");
        }
        break;
      }
    }
  }, false);

  function initSlider() {
    if (! window.location.hash.startsWith("#entry")) {
      return
    };

    let elem = document.querySelector(window.location.hash + " .carousel");

    if (elem) {
      new Flickity(elem, {
        cellAlign: 'left',
        contain: true,
        setGallerySize: false,
        resize: false
      });
    }
  }

  window.addEventListener('hashchange', initSlider);
  document.addEventListener('DOMContentLoaded', initSlider);



  document.querySelector(".page__wrapper").addEventListener('scroll', function(e) {
    document.body.classList.add('scroll');
  });

  let videos = document.querySelectorAll('video');

  for (let i = 0, len = videos.length; i < len; i++) {
    videos[i].addEventListener("play", function(e) {
      for (let i = 0, len = videos.length; i < len; i++) {
        if (videos[i] !== this) {
          videos[i].pause();
        }
      }
    });
  }

})();
