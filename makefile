# This file is part of Le Septantecinq JPO Generator.
# 
# Le Septantecinq JPO Generator is free software: you can redistribute it
# and/or modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
# 
# Le Septantecinq JPO Generator is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero
# General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with Le Septantecinq JPO Generator.  If not, see
# <http://www.gnu.org/licenses/>.





# Avoids unexpected shell behaviors
SHELL := /usr/bin/env bash





#############
# Variables #
#############

large := 1600x1600
medium := 0900x0900
small := 0600x0600
thumb := 0110x
theme := themes/default_2023

BUILD_PATH := site

IMG_INPUT := $(wildcard data/**/*.jpg)
LARGE_OUTPUT := $(patsubst data/%.jpg, $(BUILD_PATH)/img/%.$(large).jpg, $(IMG_INPUT))
MEDIUM_OUTPUT := $(patsubst data/%.jpg, $(BUILD_PATH)/img/%.$(medium).jpg, $(IMG_INPUT))
SMALL_OUTPUT := $(patsubst data/%.jpg, $(BUILD_PATH)/img/%.$(small).jpg, $(IMG_INPUT))
THUMB_OUTPUT := $(patsubst data/%.jpg, $(BUILD_PATH)/img/%.$(thumb).jpg, $(filter %00.jpg,$(IMG_INPUT)))

MAIN_META_OUTPUT := $(patsubst data/%.jpg, $(BUILD_PATH)/img/%.0000.json, $(filter %00.jpg,$(IMG_INPUT)))
THUMB_META_OUTPUT := $(patsubst %.jpg, %.json, $(THUMB_OUTPUT))
IMG_META_OUTPUT := $(patsubst %.jpg, %.json, $(LARGE_OUTPUT) $(MEDIUM_OUTPUT) $(SMALL_OUTPUT))
VERSION_META_OUTPUT := $(patsubst data/%.jpg, $(BUILD_PATH)/img/%.json, $(IMG_INPUT))

CSS_INPUT := $(wildcard $(theme)/static/css/*.css)
CSS_OUTPUT := $(patsubst $(theme)/static/css/%.css, $(BUILD_PATH)/static/css/%.css, $(CSS_INPUT))

FONTS_INPUT := $(wildcard $(theme)/static/fonts/*)
FONTS_OUTPUT := $(patsubst $(theme)/static/fonts/%, $(BUILD_PATH)/static/fonts/%, $(FONTS_INPUT))

JS_INPUT := $(wildcard $(theme)/static/js/*.js)
JS_OUTPUT := $(patsubst $(theme)/static/js/%, $(BUILD_PATH)/static/js/%, $(JS_INPUT))

STATIC_IMG_INPUT := $(wildcard $(theme)/static/img/*)
STATIC_IMG_OUTPUT := $(patsubst $(theme)/static/img/%, $(BUILD_PATH)/static/img/%, $(STATIC_IMG_INPUT))

FAVICON_INPUT := $(wildcard $(theme)/*.png $(theme)/*.ico $(theme)/*.webmanifest)
FAVICON_OUTPUT := $(patsubst $(theme)/%, $(BUILD_PATH)/%, $(FAVICON_INPUT))





###########
# Recipes #
###########

# Aliases
#########

all: img meta manifest html css js fonts static_img favicon
img: large medium small thumb
meta: img_meta thumb_meta main_meta

# Compiles dominamt_color script
################################

bin/dominant_color: src/dominant_color.c
	@mkdir -p $(@D)
	LANG=C gcc -o $@ -g -Wall $< `pkg-config vips --cflags --libs`

# Image versions recipes
########################

large: $(LARGE_OUTPUT)
$(BUILD_PATH)/img/%.$(large).jpg: data/%.jpg
	@mkdir -p $(@D)
	@vipsthumbnail $(abspath $<) --vips-progress -e srgb --size $(large) -o $(abspath $@)[Q=60,optimize_coding]

medium: $(MEDIUM_OUTPUT)
$(BUILD_PATH)/img/%.$(medium).jpg: data/%.jpg
	@mkdir -p $(@D)
	@vipsthumbnail $(abspath $<) --vips-progress -e srgb --size $(medium) -o $(abspath $@)[Q=60,optimize_coding]

small: $(SMALL_OUTPUT)
$(BUILD_PATH)/img/%.$(small).jpg: data/%.jpg
	@mkdir -p $(@D)
	@vipsthumbnail $(abspath $<) --vips-progress -e srgb --size $(small) -o $(abspath $@)[Q=60,optimize_coding]

# Create thumbnail (using image with `_00` suffix)
##################################################

thumb: $(THUMB_OUTPUT)
$(BUILD_PATH)/img/%.$(thumb).jpg : data/%.jpg
	@mkdir -p $(@D)
	@vipsthumbnail $(abspath $<) --vips-progress -e srgb --size $(thumb) -o $(abspath $@)[Q=82,optimize_coding,strip]


# Create json metadata for each image
#####################################

# TODO: do we need thumb versions in the img_meta script?
# If so, thumbs values should be parametric

img_meta: $(VERSION_META_OUTPUT)
$(BUILD_PATH)/img/%.json : data/%.jpg
	@mkdir -p $(@D)
	exiftool -description -json $< \
		| jq -f bin/img_meta.jq \
		> $@

# Create json metadata for the thumbnail image
##############################################

thumb_meta: $(THUMB_META_OUTPUT)
$(BUILD_PATH)/img/%.$(thumb).json : $(BUILD_PATH)/img/%.$(thumb).jpg
	@mkdir -p $(@D)
	exiftool -imagewidth -imageheight -json $< | \
		jq --arg dominant "$(shell ./bin/dominant_color $<)" -f bin/normalize_thumb_meta.jq > $@

# Compile json metadata for each serie of images
################################################

main_meta: $(MAIN_META_OUTPUT)
$(BUILD_PATH)/img/%.0000.json : data/%.jpg
	@mkdir -p $(@D)
	exiftool -description -keywords -json $< | \
		jq --arg uuid "$(shell uuidgen)" -f bin/main_meta.jq > $@

# Compile general json metadata for images
##########################################

$(BUILD_PATH)/manifest.images.json : main_meta thumb_meta img_meta
	for i in $(BUILD_PATH)/img/**/*0000.json; do \
		jq -s '[{ "meta": .[0], "thumb": .[1], "images": .[2:] }]' "$${i%/*}/"*.json > "$${i%/*}/final.json" ; \
	done; \
	jq -s add $(BUILD_PATH)/img/**/*final.json > $@

# Compile the general list of tags 
##################################

$(BUILD_PATH)/manifest.tags.json : $(LARGE_OUTPUT)
	mkdir -p $(@D)
	exiftool -charset utf8 -q -b -sep $$'\n' -sep $$'\n' -keywords $^ \
		| sort \
		| uniq \
		| perl -pe 'chomp if eof' \
		| jq -cRs 'split("\n") | map({"tag": ., "slug": (.|ascii_downcase)})' > $@

# Combines tags and images manifests into a json that will be used as a
# template context
#######################################################################

manifest: $(BUILD_PATH)/manifest.json
$(BUILD_PATH)/manifest.json : $(BUILD_PATH)/manifest.tags.json $(BUILD_PATH)/manifest.images.json
	mkdir -p $(@D)
	jq -s '{"tags": .[0], "entries": .[1]}' $^ > $@

# Build the html page
#####################

html: $(BUILD_PATH)/index.html  
$(BUILD_PATH)/index.html : $(BUILD_PATH)/manifest.json
	python3 bin/generate.py --templates-dir="$(theme)/templates" --outfile $@ index.html $^ 

# Copy the assets
#################

css: $(CSS_OUTPUT)
$(BUILD_PATH)/static/css/%.css : $(theme)/static/css/%.css
	mkdir -p $(@D)
	cp $^ $@

fonts: $(FONTS_OUTPUT)
$(BUILD_PATH)/static/fonts/% : $(theme)/static/fonts/%
	mkdir -p $(@D)
	cp $^ $@

js: $(JS_OUTPUT)
$(BUILD_PATH)/static/js/% : $(theme)/static/js/%
	mkdir -p $(@D)
	cp $^ $@

static_img: $(STATIC_IMG_OUTPUT)
$(BUILD_PATH)/static/img/% : $(theme)/static/img/%
	mkdir -p $(@D)
	cp $^ $@

favicon: $(FAVICON_OUTPUT)
$(BUILD_PATH)/% : $(theme)/%
	mkdir -p $(@D)
	cp $^ $@

.PHONY: serve
serve:
	python -m http.server --directory $(BUILD_PATH)

.PHONY: check
check:
	@echo "Files with missing description"
	@exiftool -filepath -if 'not $$description' -q -s3 -r data
	@echo
	@echo "***************************************************"
	@echo
	@echo "Files with missing description"
	@exiftool -filepath -if 'not $$keywords' -q -s3 -r data

.PHONY: debug
debug:
	@echo $(FAVICON_INPUT)

.PHONY: clean
clean:
	rm -fr $(BUILD_PATH)
